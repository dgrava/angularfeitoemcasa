var app = angular.module("app", []);

app.controller("redditController", function($scope, redditService, $interval, ordenacao, atualizacao){
	$scope.ordenacoes = ordenacao.listar();
	$scope.atualizacoes = atualizacao.listar();

	$scope.posts = [];
	$scope.favoritados = [];
	$scope.limite = null;
	$scope.atualizacaoAtivada = null;
	
	$scope.ordem = $scope.ordenacoes[0];
	$scope.atualizacao = $scope.atualizacoes[0];

	$scope.$watch('atualizacao', function(data) {
		$interval.cancel($scope.atualizacaoAtivada);	

		if (data.time) {
			$scope.atualizacaoAtivada = $interval(carregarDados, data.time);
		}
		
	});

	var carregarDados = function() {
		$scope.posts = [];
		redditService.posts()
		.success(function(posts) {
			$scope.posts = posts.data.children;
			$scope.limite = $scope.posts.length;
		});	
	}
	

	$scope.favoritar = function(post) {
		$scope.favoritados.unshift(post);

		$scope.posts = _.reject($scope.posts, function(p) {
			return _.isEqual(p, post);
		});
	}

	$scope.desfavoritar = function(post) {
		$scope.posts.unshift(post);

		$scope.favoritados = _.reject($scope.favoritados, function(p) {
			return _.isEqual(p, post);
		});
	}

	$scope.comLimite = function(limite) {
		$scope.limite = limite;
	}

	$scope.fimRenderizacao = function() {
		console.log("alalalalal");
	}

	carregarDados();

});


app.directive('reddit', function() {
	return {
		restrict: 'E',
		templateUrl: 'template.html',
		transclude: true
		
	}

});

app.factory("redditService", function($http) {
	return {
		posts : function() {
			return $http.get("http://www.reddit.com/r/hot.json?limit=50");
		}
	}

});

app.service('ordenacao', function() {
	this.listar = function() {

		return	 [
					{ "ordem" : "Titulo" , "orderBy" : "data.title", "reverse": false },
					{ "ordem" : "Ups" , "orderBy" : "data.ups", "reverse": true},
					{ "ordem" : "downs" , "orderBy" : "data.downs", "reverse": true}
				]

	}
});

app.service('atualizacao', function() {
	this.listar = function() {

		return	[
					{"atualizacao" : "desativada", time : null},
					{"atualizacao" : "5 seg"     , time : 5000},
					{"atualizacao" : "10 seg"    , time : 10000}
				]

	}
});

app.directive("onRepeatComplete", function( $timeout ) {
    		return {
			        restrict: 'A',
			        link: function (scope, element, attr) {
			            console.log(scope.$index)
			            if (scope.$last) {
			               $timeout(function() { 
			               	console.log("fimRenderizacao");
			               	scope.$eval(attr.repeatComplete);
						   },0);
			            }
			        }
			    }
        });
